declare MAX_PATH_LENGTH=40
declare GIT_PS1_SHOWDIRTYSTATE=1
#declare GIT_PS1_SHOWSTASHSTATE=1
#declare GIT_PS1_SHOWUNTRACKEDFILES=1
#declare GIT_PS1_SHOWUPSTREAM="auto"

declare COLOR_PATH="\[\e[34;1m\]"
declare COLOR_EXTRAS="\[\e[1;36m\]"
declare COLOR_HOST=""
declare COLOR_USER=""
declare COLOR_ROOT="\[\e[1;31;40m\]"
declare COLOR_RESET="\[\e[0m\]"

function prompt_path {
  local path="$1"
  local prefix

  [[ "$path" =~ (~) ]] && prefix="~/"

  if (( ${#path} > $MAX_PATH_LENGTH )); then
    path=${path: -$(($MAX_PATH_LENGTH - ${#prefix}))}
    [[ "$path" =~ ^[^/]*/(.*) ]] && path="$prefix(...)${BASH_REMATCH[1]}"
  fi

  echo $path
}

function prompt_jobs {
  local num_jobs=$1
  local jobs_str

  case $num_jobs in
    (0) return ;;
    (1) jobs_str="job" ;;
    (*) jobs_str="jobs" ;;
  esac

  echo "  [$num_jobs $jobs_str]"
}

function prompt_git {
  [[ -z $(type -t __git_ps1) ]] && return

  local branch="$(__git_ps1 '%s')"

  [[ -n "${branch}" ]] && echo "  [${branch}]"
}

PROMPT_COMMAND='echo -ne "\033]2;${USER}@${HOSTNAME}\007"'

declare COLOR_BY_ID

case $EUID in
  (0) COLOR_BY_ID=$COLOR_ROOT ;;
  (*) COLOR_BY_ID=$COLOR_USER ;;
esac

TITLE="\[\e]0;$(prompt_path '\w')\a\]"

PS1="${TITLE}${COLOR_RESET}( ${COLOR_BY_ID}\u${COLOR_RESET}@${COLOR_HOST}\h${COLOR_RESET}:${COLOR_PATH}\$(prompt_path '\w')${COLOR_RESET}${COLOR_EXTRAS}\$(prompt_git)\$(prompt_jobs \j)${COLOR_RESET} )"$'\n\$ '


unset COLOR_PATH COLOR_EXTRAS COLOR_USER COLOR_ROOT COLOR_HOST COLOR_RESET color_by_id
# vim: set filetype=sh :
